import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

import '../model/photo.dart';
import '../pages/detail_page.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  Future<List<Photo>> _getPhoto() async {
    var data = await http.get("https://api.unsplash.com/photos/?client_id=cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0");
    
    if (data.statusCode == 200) {
      var jsonData = json.decode(data.body);

      List<Photo> photos = [];

      for(var key in jsonData) {
        Photo photo = Photo(key["id"], key["user"]["name"], key["urls"]["small"], key["urls"]["full"]);

        photos.add(photo);
      }

      return photos;
    } else {
      throw Exception('Failed to load album');
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(widget.title),
      ),
      body: Container(
        child: FutureBuilder(
          future: _getPhoto(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {

            if(snapshot.data == null) {
              return Container(
                child: Center(
                  child: Text("Loading..."),
                ),
              );
            } else {
              return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (BuildContext context, int index){
                  return new GestureDetector(
                    onTap: () {
                      Navigator.push(context,
                        new MaterialPageRoute(builder: (context) => DetailPage(snapshot.data[index]))
                      );
                    },
                    child: new Padding(
                      padding: new EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                      child: new Card(
                        elevation: 6.0,
                        shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(16.0)
                        ),
                        child: new Column(
                          children: <Widget>[
                            new ClipRRect(
                              child: new Image.network(
                                snapshot.data[index].imageSmall, 
                                loadingBuilder: (BuildContext context, Widget child, ImageChunkEvent loadingProgress) {
                                  if (loadingProgress == null) 
                                    return child;
                                  return Center(
                                    child: CircularProgressIndicator(
                                      value: loadingProgress.expectedTotalBytes != null
                                          ? loadingProgress.cumulativeBytesLoaded / loadingProgress.expectedTotalBytes
                                          : null,
                                    ),
                                  );
                                }
                              ),
                              borderRadius: BorderRadius.only(
                                topLeft: new Radius.circular(16.0),
                                topRight: new Radius.circular(16.0)
                              )
                            ),
                            new Padding(
                              padding: new EdgeInsets.all(16.0),
                              child: new Column(
                                children: <Widget>[
                                  Text(snapshot.data[index].user)
                                ]
                              ),
                            )
                          ]
                        ),
                      )
                    ),
                  );
                }
              );
            }
          },
        ),
      ),
    );
  }
}