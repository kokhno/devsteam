import 'package:flutter/material.dart';
import '../model/photo.dart';

class DetailPage extends StatelessWidget {

  final Photo photo;

  DetailPage(this.photo);

  @override
  Widget build(BuildContext contex) {
    return Scaffold(
       appBar: AppBar(
         title: Text(photo.user),
       ),
       body: Container(
         child: Image.network(
            photo.imageFull,
            fit: BoxFit.cover,
            height: double.infinity,
            width: double.infinity,
            alignment: Alignment.center,
            loadingBuilder: (BuildContext context, Widget child, ImageChunkEvent loadingProgress) {
              if (loadingProgress == null) 
                return child;
              return Center(
                child: CircularProgressIndicator(
                  value: loadingProgress.expectedTotalBytes != null
                      ? loadingProgress.cumulativeBytesLoaded / loadingProgress.expectedTotalBytes
                      : null,
                ),
              );
            }
          ),
       )
    );
  }
}