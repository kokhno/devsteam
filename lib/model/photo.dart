class Photo {
  final String id;
  final String user;
  final String imageSmall;
  final String imageFull;

  Photo(this.id, this.user, this.imageSmall, this.imageFull);
}